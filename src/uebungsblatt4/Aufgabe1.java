package uebungsblatt4;

public class Aufgabe1 {
	public static int ggt(int m, int n) {

		int rest;

		if (m <= 0 || n <= 0) {
			return 0;
		} else if (m < n) {

			rest = n % m;

			return rest == 0 ? m : ggt(m, n - m);

		} else {

			rest = m % n;

			return rest == 0 ? n : ggt(m - n, n);
		}

	}

	public static void main(String[] args) {

		System.out.println(ggt(-6, 18));

		System.out.println(ggt(6, 18));

		System.out.println(ggt(14, 4));
	}
}
