package uebungsblatt4;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.Assert;

import org.junit.jupiter.api.Test;

class Aufgabe1Test {

	@Test
	void test() {
		
		assertEquals(Aufgabe1.ggt(-6, 18),0);
		assertEquals(Aufgabe1.ggt(6, 18),6);
		assertEquals(Aufgabe1.ggt(21, 14),7);
	}

}
