package uebungsblatt1.aufgabe3;

public class Spielfeld {

	private Feld[][] felder;

	public Spielfeld(int anzZeilen, int anzSpalten) {

		felder = new Feld[anzZeilen][anzSpalten];
		for (int i = 0; i < anzZeilen; i++)
			for (int j = 0; j < anzSpalten; j++)
				felder[i][j] = new Feld(' ');

	}

	public void setzeFeld(Spieler s) {
		felder[getFreieZeile(s.getLetzterZug())][s.getLetzterZug()].setZeichen(s.getZeichen());

	}

	public void zeigeSpielfeld() {

		for (int i = 0; i < this.getAnzahlZeilen(); i++) {
			for (int j = 0; j < this.getAnzahlSpalten(); j++)
				System.out.print(felder[i][j].getZeichen() + " | ");

			System.out.println();
		}
	}

	/*
	 * �berpr�ft ob das zuletzt gemachter Zug zu einem Gewinn gef�hrt hat
	 * 
	 * 
	 */
	boolean gewonnen(Spieler s) {

		return senkrechtGewonnen(s) || waagerechtGewonnen(s) || gerade1(s) || gerade2(s);
	}

	/*
	 *  von zu letzt gesetztem Punkt l�uft erst nach links oben und rechts runter 
	 *  und schaut ob da die Zeichen von dem Spiler bereits vorhanden sind 
	 *  
	 *  @param Spieler, um zuletzt gesetzen Punkt herauszulesen sowie das Zeichen des Spielers
	 *   
	 */
	private boolean gerade1(Spieler s) {
		int z = 1;
		int spalte = s.getLetzterZug();

		// nach links oben
		for (int x = spalte - 1, y = getFreieZeile(spalte) - 1; x >= 0 && y >= 0; x--, y--)
			if (this.getFeld(y, x) == s.getZeichen()) {
				z++;
			} else {
				break;
			}

		// nach rechts runter
		for (int x = spalte + 1, y = getFreieZeile(spalte) + 1; x < this.getAnzahlSpalten()
				&& y < this.getAnzahlZeilen(); x++, y++)
			if (this.getFeld(y, x) == s.getZeichen()) {
				z++;
			} else {
				break;
			}

		
		
		if (z >= 4)
			return true;
		else
			return false;
	}

	/*
	 *  von zu letzt gesetztem Punkt l�uft erst nach links runter und rechts oben 
	 *  und schaut ob da die Zeichen von dem Spiler bereits vorhanden sind 
	 *  @param Spieler, um zuletzt gesetzen Punkt herauszulesen sowie das Zeichen des Spielers
	 *   
	 */
	
	private boolean gerade2(Spieler s) {
		int z = 1;
		int spalte = s.getLetzterZug();
		
		// nach unten
		for (int x = spalte - 1, y = getFreieZeile(spalte) + 1; y < this.getAnzahlZeilen() && x >= 0; y++, x--) {
			if (this.getFeld(y, x) == s.getZeichen()) {
				z++;
			} else {
				break;
			}

		}

		// nach oben
		for (int x = spalte + 1, y = getFreieZeile(spalte) - 1; x < this.getAnzahlSpalten() && y >= 0; x++, y--) {
			if (this.getFeld(y, x) == s.getZeichen()) {
				z++;
			} else {
				break;
			}

		}

		if (z >= 4)
			return true;
		else
			return false;
	}

	/*
	 *  von zu letzt gesetztem Punkt l�uft erst nach links und dann nach rechts 
	 *  und schaut ob da die Zeichen von dem Spiler bereits vorhanden sind 
	 *  
	 *  @param Spieler, um zuletzt gesetzen Punkt herauszulesen sowie das Zeichen des Spielers 
	 */
	private boolean waagerechtGewonnen(Spieler s) {
		int z = 1;
		int splate = s.getLetzterZug();
		
		// nach links
		for (int y = getFreieZeile(splate) - 1; y >= 0; y--)
			if (getFeld(y, splate) == s.getZeichen()) {
				z++;
			}else {
				break;
			}

		// nach rechts
		for (int y = getFreieZeile(splate) + 1; y < this.getAnzahlZeilen(); y++)
			if (getFeld(y, splate) == s.getZeichen()) {
				z++;
			}else {
				break;
			}

		if (z >= 4)
			return true;
		else
			return false;

	}

	/*
	 *  von zu letzt gesetztem Punkt l�uft erst nach oben und dann nach unten 
	 *  und schaut ob da die Zeichen von dem Spiler bereits vorhanden sind 
	 *  
	 *  @param Spieler, um zuletzt gesetzen Punkt herauszulesen sowie das Zeichen des Spielers 
	 */
	boolean senkrechtGewonnen(Spieler s) {

		int z = 1;
		int spalte = s.getLetzterZug();

		//nach oben
		for (int x = spalte - 1; x >= 0; x--)
			if (getFeld(getFreieZeile(spalte), x) == s.getZeichen())
				z++;

		//nach unten
		for (int x = spalte + 1; x < this.getAnzahlSpalten(); ++x)
			if (getFeld(getFreieZeile(spalte), x) == s.getZeichen())
				z++;
			else
				break;

		if (z >= 4)
			return true;
		else
			return false;
	}


	/*
	 * �berpr�ft, ob eine Spalte voll ist
	 * 
	 * @param Spaltennummer, in der das letze Zeichen gesetzt wurde
	 */
	public char getFeld(int i, int j) {

		return felder[i][j].getZeichen();

	}

	/*
	 * Nach jedem Zug wird �berpr�ft, ob das Spiel zu Ende ist
	 * 
	 */
	public boolean istSpielfeldVoll() {

		for (int i = 0; i < getAnzahlSpalten(); i++)
			if (istSpalteVoll(i) == false) {
				return false;
			}

		return true;
	}

	boolean istSpalteVoll(int spalte) {

		return !Character.isSpaceChar(getFeld(0, spalte));
	}

	/* ermittelt die n�chste freie Zeile f�r die �bergebene Spalte
	 * @param spalte - eine g�ltige Spalte des Spielfelds.
	 * 
	 */
	private int getFreieZeile(int spalte) {

		for (int i = this.getAnzahlZeilen() - 1; i >= 0; i--) {
			if (this.getFeld(i, spalte) == ' ')
				return i;
		}
		return 0;

	}
	public int getAnzahlZeilen() {

		return felder.length;
	}

	public int getAnzahlSpalten() {

		return felder[0].length;
	}

}
