package uebungsblatt1.aufgabe3;

import java.io.*;

public class Spieler {
	private String name ;
	private char zeichen;
	private int letzterZug;
	private BufferedReader in;
	
	
	/*
	 * Konstruktor
	 * 
	 * @param name - der Name des Spielers
	 * @param zeichen - das Zeichen, das der Spieler bekommt
	 */
	public Spieler(String name, char zeichen) {
		this.name = name;
		this.zeichen = zeichen;
	}
	
	
	/*
	 * f�hrt einen Zug an dem �bergebenen spielfeld durch.
	 * 
	 *  @param spielfeld ->  an dem Spielfeld  der Spieler seinen Zug machen soll.
	 */
	public void machDenZug(Spielfeld spielfeld) {
		
		leseZug(spielfeld);
		spielfeld.setzeFeld(this);
		
		
	}
	
	/* Hilfsmethode, um Eingaben vom Benutzer einzulesen
	 *  wenn der Wert g�ltig ist, wird es in der Variable letzerZug gespeichert
	 *  sonst wird der Benutzer nochmal zur Eingabe aufgefordert.
	 *  
	 *  @param spielfeld ->  an dem Spielfeld der Spieler seinen Zug machen soll.
	 */
	private void leseZug(Spielfeld spielfeld) {
		
		int eingabe = 0;

		do {
			try {
				 in = new BufferedReader(new InputStreamReader(System.in));
				System.out.println(
						"\n" + name + "(" + zeichen + ") ist am Zug. Bitte gib die Spalte ein: ");
				eingabe = Integer.parseInt(in.readLine());
				if (eingabe < 1 || eingabe > spielfeld.getAnzahlSpalten()) {
					System.out.println("Spaltennummer muss zwischen 1 und " + spielfeld.getAnzahlSpalten() + " liegen!");
				} else if (spielfeld.istSpalteVoll(eingabe - 1) == true) {
					System.out.println("Die Spalte ist voll!");
				} else {
					break;
				}

			} catch (IOException e) {
				e.printStackTrace();

			}
		} while (true);

		letzterZug = eingabe -1;

	}
	
	public String getName() {
		return name;
	}
	public char getZeichen() {
		return zeichen;
	}
	public int getLetzterZug() {
		return letzterZug;
	}

	
	
	

}
