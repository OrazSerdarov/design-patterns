package uebungsblatt1.aufgabe3;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;


public class Spiel {

	private Spielfeld spielfeld;
	private ArrayList<Spieler> spieler;
	private int spielerDran;
	private BufferedReader in;


	public Spiel() {

		int anzZeilen = 0;
		int anzSpalten = 0;
		spielerDran = 0;

		try {
			in = new BufferedReader(new InputStreamReader(System.in));
			anzZeilen = leseEingabenZuSpielfeldgroesse("H�he");
			anzSpalten = leseEingabenZuSpielfeldgroesse("Breite");
			spielfeld = new Spielfeld(anzZeilen, anzSpalten);

			erstelleSpieler();

		} catch (Exception e) {
			e.getStackTrace();
		}

		spielfeld.zeigeSpielfeld();
	}

	/*
	 *  Hilfsmethode, um Spieler zu erstellen.
	 */
	private void erstelleSpieler() {

		spieler = new ArrayList<Spieler>(2);
		for (int i = 0; i < 2; i++) {
			String name = "";
			char zeichen = (i % 2 == 0) ? 'o' : '+';

		
			System.out.print("Name von SpielerIn " + (char) (65 + i) + "\t:");

			try {
		
				
				name = in.readLine();
			} catch (IOException e) {
				e.getMessage();
				continue;
			}

			if (name.equalsIgnoreCase("")) {
				System.out.println("Geben Sie einen Spielernamen ein!!!");
				continue;

			}

			for (int j = 0; j < spieler.size(); j++) {

				if ((name.equalsIgnoreCase(spieler.get(j).getName()))) {
					System.out.println("Dieser Spielername existiert bereits! \nGeben Sie einen anderen Namen ein!");
					i--;
					continue;

				}

			}

			spieler.add(new Spieler(name, zeichen));

		}

	}

	/*
	 * Fordert die Spieler abwechselnd zu Eingaben solange bis jemand gewinnt
	 * oder bis das Spielfeld voll ist.
	 */
	public void spielen() {

		try {

			while (spielfeld.istSpielfeldVoll() == false) {

				spieler.get(spielerDran).machDenZug(spielfeld);

				if (spielfeld.gewonnen(spieler.get(spielerDran))) {
					System.out.println(spieler.get(spielerDran).getName() + " hat gewonnen!");
					break;
				}

				spielerDran = (spielerDran == 1) ? 0 : 1;
				spielfeld.zeigeSpielfeld();

			}
			System.out.println("Speil ist zu Ende!");
			spielfeld.zeigeSpielfeld();
			
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	
	/* Hilfsmethode, um die Eingaben zur Gr��e des zu erstellenden Spielfelds von Spielern einzulesen.
	 * 
	 * @param s - Angibt, ob die H�he oder Breite eingelesen werden muss.
	 */
	private int leseEingabenZuSpielfeldgroesse(String s) {

		int eingabe = 0;
		do {

			try {
				in = new BufferedReader(new InputStreamReader(System.in));

				System.out.println(s + " des Spielfeldes (mindestens 4)\\t:");
				eingabe = Integer.parseInt(in.readLine());
			} catch (Exception e) {
				e.getMessage();
				continue;
			}
		} while (eingabe < 4);

		return eingabe;
	}

}
