package chain_of_responsibility;

public class InternationalHandler extends AbstractHandler {

	public boolean processOrder(Order o) {

		if (o.isInternational()) {

			if (o.getWeight() <= 70.0 && o.getPacLength() <= 150 && o.getWidth() <= 70 && o.getHeight() <= 70.0) {
				o.setHandler("\nhandeled by InternationalHandler ");
				o.setPrice(30.0);
				return true;
			} else {
				o.setHandler("\n can not be handeled by InternationalHandler ");
				return false;
			}

		} else
			return this.checkNext(o);

	}

}
