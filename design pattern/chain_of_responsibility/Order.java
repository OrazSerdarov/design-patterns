package chain_of_responsibility;

import java.util.ArrayList;

public class Order {

	private Adress adress;
	private double weight;
	private double price;
	private double pacLength;
	private double width;
	private double height;
	private String handler;

	public Order( double weight, double pacLength, double width, double height, Adress a) {
		this.adress = a;
		this.weight = weight;
		this.pacLength = pacLength;
		this.width = width;
		this.height = height;

	}

	public double getWeight() {
		return weight;
	}

	public String getAdress() {
		return adress.toString();
	}

	public boolean isDomestic() {

		if (adress == Adress.DOMESTIC)
			return true;

		return false;
	}

	public boolean isInternational() {

		if (adress == Adress.INTERNATIONAL)
			return true;

		return false;
	}

	public boolean isEuropeanUnion() {

		if (adress == Adress.EUROPEAN_UNION)
			return true;

		return false;

	}

	public String toString() {

		return "[ Weight=" + this.weight + " kg," + "L=" + this.pacLength + " cm ," + "W=" + this.width + " cm, H=" + this.height
				+ ", " + this.adress.toString() + "]" + this.handler;
	}

	public double getPrice() {
		return price;
	}

	public double getPacLength() {
		return pacLength;
	}

	public double getWidth() {
		return width;
	}

	public double getHeight() {
		return height;
	}

 void setHandler(String s) {
		this.handler=s;
		
	}
	
	void setPrice(double p) {
		this.price = p;
	}

}
