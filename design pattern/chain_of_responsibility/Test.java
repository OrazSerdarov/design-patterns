package chain_of_responsibility;

public class Test {

	public static void main(String[] args) {
		
		Order o1 = new Order(1.5,15,25,10,Adress.DOMESTIC);
		Order o2 = new Order(35.0,76,50,70,Adress.INTERNATIONAL);
		Order o3 = new Order(4.5,48,19,9,Adress.DOMESTIC);
		Order o4 = new Order(9.5,80,50,100,Adress.EUROPEAN_UNION);
		Order o5 = new Order(3.5,17,30,15,Adress.DOMESTIC);

		AbstractHandler intern = new InternationalHandler();
		AbstractHandler e = new EuropeanHandler();
		AbstractHandler dl = new LargeDomesticHandler();
		AbstractHandler ds = new SmallDomesticHandler();
		
		dl.linkWith(ds);
		e.linkWith(dl);
		intern.linkWith(e);
		
		
		
		

		
		
//		.linkWith(new SmallDomesticHandler()).linkWith(new LargeDomesticHandler())
		intern.processOrder(o1);
		intern.processOrder(o2);
		intern.processOrder(o3);
		intern.processOrder(o4);
		intern.processOrder(o5);

		System.out.println(o1.toString() + "\n");
		System.out.println(o2.toString()+ "\n");
		System.out.println(o3.toString()+ "\n");
		System.out.println(o4.toString()+ "\n");
		System.out.println(o5.toString()+ "\n");
	
	}
	
	
}
