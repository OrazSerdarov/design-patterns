package chain_of_responsibility;

public class LargeDomesticHandler extends AbstractHandler {

	public boolean processOrder(Order o) {

		if (o.isDomestic()) {

			if (o.getWeight() <= 2.0 && o.getPacLength() <= 30.0 && o.getWidth() <= 30.0 && o.getHeight() <= 30.0)
				return this.checkNext(o);
			else if (o.getWeight() <= 5.0 && o.getPacLength() <= 60 && o.getWidth() <= 30 && o.getHeight() <= 15) {
				o.setHandler("\nhandeled by LargeDomesticHandler ");
				o.setPrice(6.75);
				return true;
			} else {

				o.setHandler("\n can not be handeled by any Handler ");
				return false;

			}

		}
		return false;

	}

}
