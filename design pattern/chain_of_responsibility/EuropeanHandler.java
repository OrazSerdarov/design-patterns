package chain_of_responsibility;

public class EuropeanHandler extends AbstractHandler {

	public boolean processOrder(Order o) {

		if (o.isEuropeanUnion()) {

			if (o.getWeight() <= 10.0 && o.getPacLength() <= 80.0 && o.getWidth() <= 60.0 && o.getHeight() <= 100.0) {
				o.setPrice(10.50);
				o.setHandler("\nhandeled by EuropeanUnionHandler ");
				return true;
			} else {
				o.setHandler("\n can not be handeled by EuropeanHandler ");
				return false;
			}

		} else
			return this.checkNext(o);

	}

}
