package chain_of_responsibility;

public abstract class AbstractHandler {
	
	private AbstractHandler  next =null;
	
	public void linkWith(AbstractHandler a) {
		this.next =a;
		
	}
	

	protected boolean checkNext(Order o) {
	        if (next == null) {
	            return true;
	        }
	        return next.processOrder(o);
	    }
	
	
	public abstract boolean processOrder(Order o);
	
	
	
	


}
