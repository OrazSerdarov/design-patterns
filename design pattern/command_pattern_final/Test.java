package command_pattern_final;

public class Test {

	public static void main(String [] args) {
		
	Taschenrechner t= 	new Taschenrechner (new AdditionCommand (new AdditionAction()),new  SubstructionCommand(new  SubstructionAction()), 
			new MultiplicationCommand(new MultiplicationAction()),new DivisionCommand(new DivisionAction()));
		
		TaschenrechnerView v = new TaschenrechnerView(t);
	}
}
