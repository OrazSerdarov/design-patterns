package command_pattern_final;

public class SubstructionAction {
	
	public double subtract(double a , double b) {
		return b-a;
	}

}
