package command_pattern_final;

import java.util.Stack;

public class Taschenrechner {

	private Command currentCommand;
	private Command addCommand, subCommand, mulCommand, divCommand;
	private Stack<Command> lastCommands;

	private Stack<Double> lastOperands;
	private double result;
	private String resultLabel;

	public Taschenrechner(Command c1, Command c2, Command c3, Command c4) {

		lastCommands = new Stack<Command>();
		currentCommand = null;
		lastOperands = new Stack<Double>();

		result = 0;
		resultLabel = "";

		this.addCommand = c1;
		this.subCommand = c2;
		this.mulCommand = c3;
		this.divCommand = c4;
	}

	public void setOperatorCommand(String s) {

		switch (s) {
		case "ADD":
			currentCommand = addCommand;
			resultLabel += "+";
			break;
		case "SUB":
			currentCommand = subCommand;
			resultLabel += "-";
			break;
		case "MULT":
			currentCommand = mulCommand;
			resultLabel += "*";
			break;
		case "DIV":
			currentCommand = divCommand;
			resultLabel += "/";
			break;
		default:
			// execute was invoked
			result = execute();
			resultLabel += " = " + result;
			return;
		}

		lastCommands.push(currentCommand);

	}

	private double execute() {

		double a, b;

		if (lastOperands.empty()) {
			return result;
		} else {

			a = lastOperands.pop();

			if (lastOperands.empty()) {

				

			} else {
				b = lastOperands.peek();
				result = currentCommand.execute(a, b);
				lastOperands.push(result);
				

			}
		}

		return result;
	}

	public String getResultLabel() {
		return resultLabel;
	}

	public void undo() {
		
		double a ,b;
		if(lastCommands.empty()) {
			
		}else {
			currentCommand= lastCommands.pop();
			
			a= lastOperands.pop();
			b=lastOperands.pop();
			result =currentCommand.undo(a, b);
			resultLabel+=  "  " + result;
		}

	}

	public void setOperand(double a) {

		resultLabel += " " + a;
		lastOperands.push(a);

	}

}
