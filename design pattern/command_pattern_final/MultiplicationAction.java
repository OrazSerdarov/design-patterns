package command_pattern_final;

public class MultiplicationAction {
	
	public double mult(double a, double b) {
		return a*b;
	}

}
