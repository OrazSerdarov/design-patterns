package command_pattern_final;

public interface Command {

	public double execute(double a, double b);
	public double undo( double a, double b);

}
