package command_pattern_final;

public class DivisionCommand implements Command {

	private DivisionAction divAction;

	public DivisionCommand(DivisionAction a) {
		divAction = a;

	
	}

	public double execute(double a , double b) {
	
		return divAction.divide(a, b);
	}


	public double undo(double quotient, double divident) {
		return quotient * divident;
	}

}
