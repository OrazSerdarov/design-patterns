package command_pattern_final;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;


public class TaschenrechnerView extends JFrame{
	
	
	private Taschenrechner t;
	private JButton eins, zwei, drei, vier, fuenf, sechs, sieben, acht, neun, nul, slash, mal, minus, gleich, plus;
	private JButton undo, redo;
	private JLabel resultLabel;
	
	
	public TaschenrechnerView(Taschenrechner t) {
		
		this.t = t;
		
		OperandHandler operandHandlder = new OperandHandler();
		OperationHandler oprationHanlder = new OperationHandler();
		ActionHandler actionHanlder = new ActionHandler();
		resultLabel = new JLabel("0.0", SwingConstants.RIGHT);

		JPanel p = new JPanel();
		p.setLayout(new GridLayout(4, 4));

		nul = new JButton("0");
		eins = new JButton("1");
		zwei = new JButton("2");
		vier = new JButton("4");
		drei = new JButton("3");
		fuenf = new JButton("5");
		sechs = new JButton("6");
		sieben = new JButton("7");
		acht = new JButton("8");
		neun = new JButton("9");
		
		nul.addActionListener(operandHandlder);
		eins.addActionListener(operandHandlder);	
		zwei.addActionListener(operandHandlder);
		drei.addActionListener(operandHandlder);
		vier.addActionListener(operandHandlder);
		fuenf.addActionListener(operandHandlder);
		sechs.addActionListener(operandHandlder);
		sieben.addActionListener(operandHandlder);
		acht.addActionListener(operandHandlder);
		neun.addActionListener(operandHandlder);
		
		
		slash = new JButton("/");
		mal = new JButton("*");
		minus = new JButton("-");
		redo = new JButton("redo");
		gleich = new JButton("=");
		plus = new JButton("+");
		
		undo = new JButton("undo");
		redo = new JButton("redo");
			

		undo.addActionListener(actionHanlder);
		redo.addActionListener(actionHanlder);
		
		slash.addActionListener(oprationHanlder);
		mal.addActionListener(oprationHanlder);
		minus.addActionListener(oprationHanlder);
		gleich.addActionListener(oprationHanlder);
		plus.addActionListener(oprationHanlder);
		
	

		

		p.add(sieben);
		p.add(acht);
		p.add(neun);
		p.add(plus);
		
		p.add(vier);
		p.add(fuenf);
		p.add(sechs);
		p.add(minus);

		

		p.add(eins);
		p.add(zwei);
		p.add(drei);	
		p.add(mal);	
		

		p.add(undo);
		p.add(nul);	
		p.add(redo);	
		p.add(slash);	
	

	
		

		this.add(resultLabel, BorderLayout.NORTH);
		this.add(p);
		this.add(new JPanel().add(gleich),BorderLayout.SOUTH);

		setSize(600, 800);
		this.pack();
		this.setLocation(300, 300);
		this.setVisible(true);
	}
	
	
	class OperandHandler implements ActionListener {
		public void actionPerformed(ActionEvent a) {
			Object o = a.getSource();

			if (o == nul)
				t.setOperand(0);
			if (o == eins)
				t.setOperand(1);
			if (o == zwei)
				t.setOperand(2);
			if (o == drei)
				t.setOperand(3);
			if (o == vier)
				t.setOperand(4);
			if (o == fuenf)
				t.setOperand(5);
			if (o == sechs)
				t.setOperand(6);
			if (o == sieben)
				t.setOperand(7);
			if (o == acht)
				t.setOperand(8);
			if (o == neun)
				t.setOperand(9);
			

			resultLabel.setText(t.getResultLabel());

		}
	
	
	
}
	
	class ActionHandler implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent a) {
		
			Object o = a.getSource();
			
			if(o == undo) {
				t.undo();
			}
			else if (o ==redo){
				t.setOperatorCommand("EXC");
			}
			
			resultLabel.setText(t.getResultLabel());

		}
		
		
	}
	
	
	class OperationHandler implements ActionListener {
		public void actionPerformed(ActionEvent a) {

		Object o = a.getSource();
		
			if (o == slash)
				 t.setOperatorCommand("DIV");
			if (o == mal)
				 t.setOperatorCommand("MULT");
			if (o == minus)
				 t.setOperatorCommand("SUB");
			if (o == plus)
				 t.setOperatorCommand("ADD");
			if (o == gleich)
				 t.setOperatorCommand("EXC");
			


			resultLabel.setText(t.getResultLabel());

		}
		
	
	
	
	
}
	
}
