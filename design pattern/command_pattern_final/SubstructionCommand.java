package command_pattern_final;

public class SubstructionCommand implements Command {

	private SubstructionAction subAct;

	public SubstructionCommand(SubstructionAction s) {
		this.subAct = s;

	}

	public double execute(double a, double b) {

		return subAct.subtract(a, b);
	}


	public double undo(double difference, double divisor) {
		return difference +divisor;
	}

}
