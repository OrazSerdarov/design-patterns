package command_pattern_final;

public class AdditionAction {

	public double add(double x , double y) {
		
		return x+y;
	}
	
}
