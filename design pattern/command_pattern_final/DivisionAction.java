package command_pattern_final;

public class DivisionAction {
	
	public double divide(double a, double b) {
		
		return b/a;
	}

}
