package command_pattern_final;

public class AdditionCommand implements Command {
	
	private AdditionAction addAct;

	
	public AdditionCommand(AdditionAction a) {
		addAct = a;
	
	}
	
	public double execute(double a, double b) {
		return addAct.add(a, b);
	}


	public double undo(double sum , double b) {
	
		return  sum -b;
	}

}

