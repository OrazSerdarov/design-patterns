package command_pattern_final;

public class MultiplicationCommand implements Command {
	
	private MultiplicationAction multAct;


	
	public MultiplicationCommand(MultiplicationAction a ) {
		this.multAct= a;

	}
	public double execute(double a, double b) {
	
		return multAct.mult(a, b);
	}



	public double undo( double product, double multiplier) {

		return product / multiplier ;
	}

	
	

}
