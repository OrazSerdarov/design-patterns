package abstract_factory_pattern_initial;

public class PizzaOnlineStore {

	public Pizza orderPizza(String pizzaType) {
		Pizza pizza = null;
		switch (pizzaType) {
		case "Cheese":
			pizza = new CheesePizza();
			break;
		case "Vegetarian":
			pizza = new VegetarianPizza();
			break;
		case "Calzone":
			pizza = new CalzonePizza();
			break;
		}
		pizza.prepare();
		pizza.bake();
		pizza.cut();
		pizza.box();
		return pizza;
	}

}
