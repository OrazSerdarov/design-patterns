package abstract_factory_pattern_initial;

public interface Pizza {
	


	
	public void prepare();
	public  void bake();
	public void cut();
	public  void box() ;
	public String toString();
}
