package abstract_factory_pattern_initial;

public class PizzaOnlineStoreTest {

	public static void main(String[] args) {

		PizzaOnlineStore store = new PizzaOnlineStore();
		
		Pizza pizza = store.orderPizza("Cheese");
		System.out.println("We ordered a " + pizza);
		System.out.println();
		
		
		pizza = store.orderPizza("Vegetarian");
		System.out.println("We ordered a " + pizza);
		System.out.println();
		
		
		pizza = store.orderPizza("Calzone");
		System.out.println("We ordered a " + pizza);
		System.out.println();
		
	}

}
