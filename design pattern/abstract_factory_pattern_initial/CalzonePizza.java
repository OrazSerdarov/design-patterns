package abstract_factory_pattern_initial;

public class CalzonePizza implements Pizza {
	


	public void prepare() {

		System.out.println("Preparing 6 min Calzone Pizza");
		
	}


	public void bake() {	
		System.out.println("Baking  30 min at 350� Calzone");
	
		
	}


	public void cut() {
		System.out.println("No Cut Calzone");
		
	}


	public void box() {
		System.out.println("Choosing special box for Calzone");
		
	}

	public String toString() {
		return "----Calzone----";
	}

}
