package abstract_factory_pattern_initial;

public class CheesePizza implements Pizza{
	
	

	public void prepare() {
		System.out.println("Preparing 5 min Cheese Pizza");
		
	}

	@Override
	public void bake() {
		System.out.println("Baking 25 min at 350�Cheese");
			
	}

	@Override
	public void cut() {
	
		System.out.println("Cut Cheese");
			
	}

	@Override
	public void box() {

		System.out.println("Boxing Cheese");
			
	}
	public String toString() {
		return "----Cheese----";
	}


}
