package abstract_factory_pattern_initial;

public class VegetarianPizza implements Pizza {

	public void prepare() {

		System.out.println("Preparing 3 min Vegetarian Pizza");
	}

	@Override
	public void bake() {
		System.out.println("Baking 20 min at 350� Vegetarian");

	}

	public void cut() {
		System.out.println("Cut Vegetarian");

	}

	@Override
	public void box() {
		System.out.println("Boxing Vegetarian");


	}
	
	public String toString() {
		return "----Vegetarian----";
	}


}
