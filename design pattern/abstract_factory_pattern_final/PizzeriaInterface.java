package abstract_factory_pattern_final;

public interface PizzeriaInterface {
	
	
	public abstract PizzaProduct createPizza(String name);
	public abstract PizzaProduct createPasta(String name);

}
