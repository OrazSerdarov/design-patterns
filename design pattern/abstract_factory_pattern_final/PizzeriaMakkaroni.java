package abstract_factory_pattern_final;

public class PizzeriaMakkaroni implements PizzeriaInterface{




	public PizzaProduct createPizza(String name) {
		PizzaProduct p = null;
		
		switch(name) {
		case "CheesePizza": 
				p = new CheesePizzaMakkaroni();
				break;
		case "Vegetarian":
			p = new VegetarianPizzaMakkaroni();
			break;
		default:
			return null;
		}
		p.prepare();
		p.bake();
		p.cut();
		p.box();
		
		return p;
	

		}
	

	@Override
	public PizzaProduct createPasta(String name) {
		// TODO Auto-generated method stub
		return null;
	}

}
