package abstract_factory_pattern_final;

public class VegetarianPizzaAlberto extends PizzaProduct{
	
	public VegetarianPizzaAlberto() {
		super("VegetarianPizza");
		this.addTopping("Vegetarian Alberto Topping1");
		this.addTopping("Vegetarian Alberto Topping2");
		this.addTopping("Vegetarian Alberto Topping3");
		
	}

	public void prepare() {

		System.out.println("Preparing 3 min Vegetarian Pizza");
	}

	public void bake() {
		System.out.println("Baking 20 min at 350� Vegetarian");

	}

	public void cut() {
		System.out.println("Cut Vegetarian");

	}


	public void box() {
		System.out.println("Boxing Vegetarian");


	}
	
	public String toString() {
		return "----Vegetarian Alberto----";
	}




}
