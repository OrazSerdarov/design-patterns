package abstract_factory_pattern_final;

public class CheesePizzaAlberto extends PizzaProduct{
	
	
	public CheesePizzaAlberto() {
		super("CheesePizza");
		this.addTopping("MozzarellaCheese");
		this.addTopping("Tomato sauce");
		this.addTopping("Oregano");
	
}


	public void prepare() {
		System.out.println("Preparing 5 min Cheese Pizza");
		
	}

	public void bake() {
		System.out.println("Baking 25 min at 350�Cheese");
			
	}

	public void cut() {
	
		System.out.println("Cut Cheese");
			
	}

	public void box() {

		System.out.println("Boxing Cheese");
			
	}
	public String toString() {
		return "----Cheese Alberto----";
	}

}
