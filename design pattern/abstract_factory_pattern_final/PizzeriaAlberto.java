package abstract_factory_pattern_final;

public class PizzeriaAlberto implements  PizzeriaInterface{




	public PizzaProduct createPizza(String name) {
		PizzaProduct p = null;
		
		switch(name) {
		case "CheesePizza":
			p = new CheesePizzaAlberto();
			break;
		case "Vegetarian": 
			p= new VegetarianPizzaAlberto();
			break;
		default:
			return null;
		}
		
		p.prepare();
		p.bake();
		p.cut();
		p.box();
		
		return p;
	
	}

	@Override
	public PizzaProduct createPasta(String name) {
		// TODO Auto-generated method stub
		return null;
	}

}
