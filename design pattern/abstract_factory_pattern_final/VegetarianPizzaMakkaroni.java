package abstract_factory_pattern_final;

public class VegetarianPizzaMakkaroni extends PizzaProduct {

	
	public VegetarianPizzaMakkaroni() {
		super("VegetarianPizza");
		this.addTopping("Vegetarian Makkaroni Topping1");
		this.addTopping("Vegetarian Makkaroni Topping2");
		this.addTopping("Vegetarian Makkaroni Topping3");
	}

	public void prepare() {

		System.out.println("Preparing 3 min Vegetarian Pizza");
	}

	public void bake() {
		System.out.println("Baking 20 min at 350� Vegetarian");

	}

	public void cut() {
		System.out.println("Cut Vegetarian");

	}


	public void box() {
		System.out.println("Boxing Vegetarian");


	}
	
	public String toString() {
		return "----Vegetarian Makkaroni----";
	}


}
