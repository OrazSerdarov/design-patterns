package abstract_factory_pattern_final;

import java.util.ArrayList;

public abstract class PizzaProduct {
	
	protected String name;
	protected ArrayList<String> toppings; 
	
	

	protected PizzaProduct(String n) {
		name= n;
		toppings = new ArrayList<String>(5);
	}
	
	protected void addTopping(String s) {
		toppings.add(s);
	}
	public void toppings() {
		for(String s : toppings) {
			System.out.println(s);
		}
	}
	public abstract void prepare();
	public abstract void bake();
	public abstract void cut();
	public abstract void box();
	
}
