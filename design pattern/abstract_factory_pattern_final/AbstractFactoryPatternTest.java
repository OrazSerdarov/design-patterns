package abstract_factory_pattern_final;

public class AbstractFactoryPatternTest {

	public static void main(String[] args) {

		PizzeriaInterface pizzeriaMakkaroni  = new PizzeriaMakkaroni();
		PizzaProduct pizzaMakkaroniCheese= pizzeriaMakkaroni .createPizza("CheesePizza");
		System.out.println(pizzaMakkaroniCheese + "\nincludes Toppings:");
		pizzaMakkaroniCheese.toppings();
		
		System.out.println("---------------------------");
		
		

		PizzeriaInterface pizzeriaAlberto  = new PizzeriaAlberto();
		PizzaProduct pizzaAlbertoCheese = pizzeriaAlberto.createPizza("CheesePizza");
		System.out.println(pizzaAlbertoCheese + "\nincludes Toppings: ");
		pizzaAlbertoCheese.toppings();
	}

}
