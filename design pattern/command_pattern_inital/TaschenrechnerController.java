package command_pattern_inital;

import command_pattern_final.Command;

public class TaschenrechnerController {
	
	enum operation {ADD,SUB,MULT,DIV};
	private Command command;
	private operation currentOperation;
	private double operand1, operand2;
	private double result;
	
	public TaschenrechnerController() {
		command=null;
	}

	public void setOperation(operation o) {
		currentOperation =o;
	}
	
	public void action() {
		
		switch(this.currentOperation) {
			
		}
		
		
	}

}
