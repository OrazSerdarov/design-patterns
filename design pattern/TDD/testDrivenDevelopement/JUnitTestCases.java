package TDD.testDrivenDevelopement;

import junit.framework.*;
import org.junit.Test;

public class JUnitTestCases extends TestCase{
	
	@Test
	public void testLessThan140() throws Exception{			//139 Zeichen
		System.out.println("test0");
		String s = "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, se";
		assertOutcome(s, s);
	}
	
	@Test
	public void testMoreThan140Case1() throws Exception{			//141 Zeichen
		System.out.println("test1");
		String s = "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sedd";
		String outcome = "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat,...";
		assertOutcome(outcome, s);
	}
	
	@Test
	public void testMoreThan140Case2() throws Exception{			//sehr langes Wort am Ende
		System.out.println("test2");
		String s = "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam Llanfairpwllgwyngyllgogerychwyrndrobwllllantysiliogogogoch";
		String outcome = "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam...";
		assertOutcome(outcome, s);
	}
	
	@Test
	public void testMoreThan140Case3() throws Exception{			//viele kurze W�rter am Ende (140 Zeichen PLUS "...")
		System.out.println("test3");
		String s = "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At v";
		String outcome = "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed...";
		assertOutcome(outcome, s);
	}
	
	@Test
	public void testEmpty() throws Exception{			//leere Zeichenkette
		System.out.println("test4");
		String s = "";
		assertOutcome(s, s);
	}
	
	@Test
	public void testOneLongWord() throws Exception{			//ein langes Wort (gibt es nur ein Wort wird das gek�rzt)
		System.out.println("test5");
		String s = "LoremipsumdolorsitametconsetetursadipscingelitrseddiamnonumyeirmodtemporinviduntutlaboreetdoloremagnaaliquyameratseddiamvoluptuaAtveroeosetlttttt";
		String outcome = "LoremipsumdolorsitametconsetetursadipscingelitrseddiamnonumyeirmodtemporinviduntutlaboreetdoloremagnaaliquyameratseddiamvoluptuaAtveroeosetl...";
		assertOutcome(outcome, s);		//nicht ganz eindeutig aus der Aufgabenstellung ableitbar
	}
	
	private void assertOutcome(String expectedOutcome, String input) {
		String st = Utilities.shortenText(input);					//anscheinend soll die getestet werden
		System.out.println(st);
		assertTrue(st.equals(expectedOutcome));						//sonst kommt ein leerzeichen am anfang zur�ck
	}

}
