package TDD.testDrivenDevelopement;

import java.util.StringTokenizer;

public class Utilities {
	
	//CODE VON JOHANNES WOLLNIK UND ORAZ SERDAROV
	
	public static String prepareStringForUrl(String s) {
		
		String ausgabe = "";
		
		for(int i = 0; i<s.length();i++) {
			Character c = s.charAt(i);
			int j = (int) c;
			
			if(j == 63 || j == 35 || j == 38)		//convert ?, & und #
				ausgabe += convertToEntity(j);
			else if(j >= 65 && j <= 90)				//convert uppercase to lowercase
				ausgabe += (char)(j + 32);
			else if(j <= 47 || (j >= 58 && j <= 64) || (j >= 91 && j <= 96) || j >= 123) { 	//convert Sonderzeichen to Minus
				ausgabe += (char) 45;
				ausgabe = deleteDoubleMinus(ausgabe);
			}
			else									//Kleinbuchstaben und Ziffern werden übernommen
				ausgabe += (char) j;
					
		}
		
		
		if(ausgabe.startsWith("-"))								//delete first and last Minus
			ausgabe = ausgabe.substring(1, ausgabe.length());
		if(ausgabe.endsWith("-"))
			ausgabe = ausgabe.substring(0, ausgabe.length()-1);
		
		if (ausgabe.length() == 0)		//Fehler-Fall
			return null;
		
		return ausgabe;
		
	}

	private static String convertToEntity(int i) {				//Hilfsmethode
		
		if(i == 63)
			return "qm";
		else if(i == 35)
			return "hash";
		else 
			return "amp";
		
	}
	
	private static String deleteDoubleMinus(String s) {			//Hilfsmethode, entfernt die Doppel-Minus
		int l = s.length();
		
		if(l > 1) {
			if(s.charAt(l-1) == s.charAt(l-2))
				s = s.substring(0, l-1);
		}
		
		return s;
			
	}
	
	
	
	
	
	//CODE VON MICHAEL BEDENK UND BENEDIKT BENTELE
	
	 public static String shortenText(String input) {
	        /*
	         * max 140 chars '...' if shortened if possible don't shorten a word itself
	         * text... <- example for a result
	         */

	        if (input == null) {
	            return null;
	        } else {
	            StringBuilder outputStringBuilder = new StringBuilder();

	            StringTokenizer tokenizer = new StringTokenizer(input, " ");
	            while (tokenizer.hasMoreElements()) {
	                String token = (String) tokenizer.nextElement();

	                if (outputStringBuilder.length() + 1 + token.length() > 140) {
	                    if (outputStringBuilder.length() == 0) {
	                        // didn't add anything so 140 is the one
	                        return input.substring(0, 140) + "...";
	                    } else {
	                        outputStringBuilder.append("...");
	                        return outputStringBuilder.toString();
	                    }
	                } else {
	                	if(outputStringBuilder.length() != 0)
	                		outputStringBuilder.append(' ');
	                    outputStringBuilder.append(token);
	                }
	            }

	            return outputStringBuilder.toString();
	        }
	    }

	    public static String shortenTextWithSpace(String input) {
	        /*
	         * max 140 chars '...' if shortened if possible don't shorten a word itself
	         * text... <- example for a result
	         */

	        if (input == null) {
	            return null;
	        }

	        if (input.length() <= 140) {
	            return input;
	        } else {
	            char[] chars = input.toCharArray();
	            int currentPosition = 140;

	            while (checkOne(currentPosition, chars) || checkTwo(currentPosition, chars)) {
	                currentPosition--;
	            }

	            if (currentPosition == -1) {
	                return input.subSequence(0, 140).toString() + "...";
	            }

	            return input.subSequence(0, currentPosition).toString() + "...";
	        }
	    }

	    private static boolean checkOne(int current, char[] charArray) {
	        return current >= 0 && charArray[current] != ' ';
	    }

	    private static boolean checkTwo(int current, char[] charArray) {
	        return current - 1 >= 0 && charArray[current - 1] == ' ';
	    }
	
}
