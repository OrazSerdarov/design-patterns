package decorator_pattern;

public class Pickles extends Accompaniment {
	
	public Pickles(Dish d ) {
		super(d);
	}


	
	public String topping() {

		StringBuilder s = new StringBuilder(dish.topping());

		return s.append(" with Pickles").toString();

	}


	public double getCosts() {
		

		
		return  0.1 +  dish.getCosts()  ;
	}

}
