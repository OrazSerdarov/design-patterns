package decorator_pattern;

public class Cheese extends Accompaniment{

	
	public Cheese(Dish d) {
		super(d);

	}
	
	public String topping() {

		StringBuilder s = new StringBuilder(dish.topping());

		return s.append(" with Cheese").toString();

	}

	public double getCosts() {
		
		return dish.getCosts() + 0.5;
	}


}
