package decorator_pattern;

public class Meat extends Accompaniment {

	public Meat(Dish d) {
		super(d);

	}

	public String topping() {

		StringBuilder s = new StringBuilder(dish.topping());

		return s.append(" with Meat").toString();

	}

	public double getCosts() {

		return dish.getCosts() + 0.6;
	}

}
