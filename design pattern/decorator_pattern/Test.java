package decorator_pattern;

public class Test {

	public static void main(String[] args) {
	
		
		Dish b = new Bread(new Sandwich());
		Dish bc = new Cheese(new Bread(new Sandwich()));
		Dish bcm = new Meat(new Cheese(new Bread(new Sandwich())));
		Dish bcmp = new Pickles(new Meat(new Cheese(new Bread(new Sandwich()))));
		Dish bcmpc = new Cheese(new Pickles(new Meat(new Cheese(new Bread(new Sandwich())))));
		
		System.out.println(b.topping() + " Price:" + b.getCosts() + "$");
		System.out.println(bc.topping() + " Price:" + bc.getCosts() + "$");
		System.out.println(bcm.topping() + " Price:" + bcm.getCosts() + "$");
		System.out.println(bcmp.topping() + " Price:" + bcmp.getCosts() + "$");
		System.out.println(bcmpc.topping() + " Price:" + bcmpc.getCosts() + "$");
		
	}

}
 