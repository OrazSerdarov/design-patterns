package decorator_pattern;

public  class Bread extends Accompaniment{

	
	
	public Bread(Dish d ) {
		super(d);

	}


	public String topping() {

		StringBuilder s = new StringBuilder(dish.topping());

		return s.append(" with Bread ").toString();

	}



	public double getCosts() {
		
		return dish.getCosts() + 0.5;
	}



}
