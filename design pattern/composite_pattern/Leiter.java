package composite_pattern;

import java.util.ArrayList;

public class Leiter extends Mitarbeiter {
	

	private ArrayList<Mitarbeiter> untergeordneteMitarbeiter;

	public Leiter(String name, String vorname, int perNummer, String abteilung, String zustaendigkeit, double gehalt) {
		super(name, vorname, perNummer, abteilung, zustaendigkeit, gehalt);

		untergeordneteMitarbeiter = new ArrayList<Mitarbeiter>(10);
	}
	
	
	//ist der Mitarbeiter selbst ein Leiter, werden seine untergeodnete Mitarbeiter erst dem neuen Leiter 
	//Leiter zugeodnet und dann erst der Leiter entfernt.
	public void mitarbeiterEntfernen(int perNummer) {

		for (int i = 0; i < untergeordneteMitarbeiter.size(); i++) {

			if (untergeordneteMitarbeiter.get(i).getPerNummer() == perNummer) {

				if (untergeordneteMitarbeiter.get(i).getMitarbeiter().size() == 0) {
					untergeordneteMitarbeiter.remove(i);
				} else {

					ArrayList<Mitarbeiter> l = untergeordneteMitarbeiter.get(i).getMitarbeiter();
					for (int j = 0; j < l.size(); j++) {
						untergeordneteMitarbeiter.add(l.get(j));
					}
					
					untergeordneteMitarbeiter.remove(i);

				}

			}

		}

	}

	// f�gt einen Mitarbeiter hinzu.
	public void mitarbeiterHinzufuegen(Mitarbeiter m) {
		untergeordneteMitarbeiter.add(m);
	}

	// print-Methode 
	public void ausgeben() {

		super.ausgeben();
		System.out.println("Zugeordnete Mitarbeiter: \n[");
		for (Mitarbeiter m : untergeordneteMitarbeiter) {
			System.out.print("\t");
			m.ausgeben();

		}
		System.out.println("]");
	}
	
	public ArrayList<Mitarbeiter> getMitarbeiter() {

		return untergeordneteMitarbeiter;
	}


}
