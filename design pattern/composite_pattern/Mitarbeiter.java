package composite_pattern;

import java.util.ArrayList;

 abstract class Mitarbeiter {
	protected String vorname;
	protected String name;
	protected int perNummer;
	protected double gehalt;
	protected String zustaendigkeit;
	protected String abteilung;
	
	public Mitarbeiter( String name ,String vorname , int perNummer ,  String abteilung , String zustaendigkeit , double gehalt ) {
		this.vorname = vorname;
		this.name = name;
		this.perNummer = perNummer;
		this.gehalt = gehalt;
		this.zustaendigkeit = zustaendigkeit;
		this.abteilung = abteilung;
	}
	
	//transparante Methode d.h. AtomareMitarbeiter geben einen leeren Array zurück.
	public abstract ArrayList<Mitarbeiter> getMitarbeiter();

	
	public void ausgeben() {
		System.out.println("Name: " + getName() + getVorname() + "  Personalnummer: " + getPerNummer() + "  Abteilung: "
				+ getAbteilung() + "  Zuständigkeit:  " + getZustaendigkeit() + "  Gehalt: " + getGehalt());
		
	}
	

	public String getAbteilung() {
		return abteilung;
	}
	public void setAbteilung(String abteilung) {
		this.abteilung = abteilung;
	}

	public String getVorname() {
		return vorname;
	}
	public void setVorname(String vorname) {
		this.vorname = vorname;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getPerNummer() {
		return perNummer;
	}
	public void setPerNummer(int perNummer) {
		this.perNummer = perNummer;
	}
	public double getGehalt() {
		return gehalt;
	}
	public void setGehalt(float gehalt) {
		this.gehalt = gehalt;
	}
	public String getZustaendigkeit() {
		return zustaendigkeit;
	}
	public void setZustaendigkeit(String zustaendigkeit) {
		this.zustaendigkeit = zustaendigkeit;
	}
	
//	public abstract void mitarbeiterEntfernen(int perNummer);
	//public abstract void mitarbeiterHinzufuegen(Mitarbeiter m) ;
}
