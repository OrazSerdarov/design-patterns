package composite_pattern;

public class Test {

	public static void main(String[] args) {
		
		
		Leiter l1 = new Leiter( "REICH", "Egon" , 258762 ,"MANAGEMENT" ," Gesch�fts-f�hrer" , 40239.39);
		Leiter l2 = new Leiter( "FAUL", "Lars" , 304909  ,"MEDIEN" ,"Medienabteilung" , 4020.03);
		Mitarbeiter m1 = new AtomareMitarbeiter( "Schmitt", "Konrad" , 30000  ,"MEDIEN" ,"Sachbearbeiter" , 2020.0);
	
		
		Mitarbeiter m2 = new AtomareMitarbeiter( "Meyer", "Peter" , 32000  ,"MEDIEN" ,"Sachbe-arbeiter " , 2000.0);
		
		l1.mitarbeiterHinzufuegen(l2);
		l2.mitarbeiterHinzufuegen(m1);
		l2.mitarbeiterHinzufuegen(m2);

		l1.ausgeben();
		
		l1.mitarbeiterEntfernen(304909);
		
		System.out.println("----------------------------------");
		l1.ausgeben();
		
	}

}
