package state_pattern;

public class GarageDoorTest {

	public static void main(String[] args) {
		GarageDoor g = new GarageDoor();
		GarageDoor g1 = new GarageDoor();
		GarageDoor g2 = new GarageDoor();
		
		
		System.out.println("Garage1 lock");
		g.lock();
		System.out.println("Garage2 lock");
		g1.lock();
		System.out.println("Garage1 startUnlock");
		g.startUnlock();
		System.out.println("Garage2 startUnlock");
		g1.startUnlock();	
		

		System.out.println("---------------------------------\nGarage3 ");
		g2.lock();
		g2.open();
		g2.startUnlock();
		g2.combinationEntered();
		g2.open();
	}

}
