package state_pattern;

public interface GarageState {
	
	public void close(GarageDoorInterface g);
	public void open(GarageDoorInterface g);
	public void lock(GarageDoorInterface g);
	public void startUnlock(GarageDoorInterface g);
	public void errorEntered(GarageDoorInterface g);
	public void combinationEntered(GarageDoorInterface g);
	

	
	
	

}
