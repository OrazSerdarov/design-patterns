package state_pattern;

public class GarageAwaitingCombination implements GarageState {
	
	
	private static GarageState awaitingCombination  = null;
	
	private GarageAwaitingCombination() {
		System.out.println("GarageAwaitingCombination State created ! ");
		
	}
	
	public static GarageState enter() {

		if(awaitingCombination ==null)
			awaitingCombination=new GarageAwaitingCombination();
		
		return awaitingCombination;
	}

	
	public void errorEntered(GarageDoorInterface garageDoor) {
		System.out.println("Incorrect combination is  entered.");
		garageDoor.setGarageState(GarageLocked.enter());
		
	}
	public void combinationEntered(GarageDoorInterface garageDoor) {
		
		System.out.println("Combination is successfully entered");
		garageDoor.setGarageState(GarageClosed.enter());
	}

	public void close(GarageDoorInterface garageDoor) {

		System.out.println("Garage is in AwaitingCombination state.");
	}

	public void open(GarageDoorInterface garageDoor) {
		System.out.println("Garage is in AwaitingCombination state.");
	}


	public void lock(GarageDoorInterface garageDoor) {
		System.out.println("Garage is in AwaitingCombination state.");
	}


	public void startUnlock(GarageDoorInterface garageDoor) {

		System.out.println("Garage is in AwaitingCombination state.");
	}


}
