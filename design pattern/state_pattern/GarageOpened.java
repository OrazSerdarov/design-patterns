package state_pattern;

public class GarageOpened implements GarageState {
	
	private static GarageState garageOpened  = null;
	
	private GarageOpened() {
		System.out.println("GarageOpened State created ! ");
		
	}
	public static GarageState enter() {

		if(garageOpened ==null)
			garageOpened=new GarageOpened();
		
		return garageOpened;
	}
	
	
	public void close(GarageDoorInterface garageDoor) {
		System.out.println("Garage will be closed now...");
		garageDoor.setGarageState(GarageClosed.enter());
	}


	public void open(GarageDoorInterface garageDoor) {
		System.out.println("Garage is in opened state.");
		
	}

	public void lock(GarageDoorInterface garageDoor) {
		System.out.println("Garage is in opened state.");
		
		
	}
	
	public void startUnlock(GarageDoorInterface garageDoor) {
		System.out.println("Garage is in opened state.");
		
		
	}


	public void errorEntered(GarageDoorInterface garageDoor) {
		System.out.println("Garage is in opened state.");
		
		
	}

	public void combinationEntered(GarageDoorInterface garageDoor) {
		System.out.println("Garage has to be locked first..");
		
	}

}
