package state_pattern;

public class GarageDoor implements GarageDoorInterface{
	
	GarageState currentState ;

	public GarageDoor() {

		currentState = GarageClosed.enter();
	}

	
	public void close() {
		currentState.close(this);
	}
	public void open() {
		currentState.open(this);
	}
	public void lock() {
		currentState.lock(this);
	}
	public void startUnlock() {
		currentState.startUnlock(this);
	}
	public void errorEntered() {
		currentState.errorEntered(this);
	}
	public void combinationEntered() {
		 currentState.combinationEntered(this);
	}	
	
	
	public void setGarageState(GarageState s) {
		currentState=s;
	}
	public GarageState getCurrentState() {
		return currentState;
	}
	

}
