package state_pattern;

public class GarageLocked implements GarageState {
	
	
	private static GarageState garageLocked  = null;
	
	private GarageLocked() {
		System.out.println("GarageLocked State created ! ");
		
	}
	public static GarageState enter() {

		if(garageLocked ==null)
			garageLocked=new GarageLocked();
		
		return garageLocked;
	}
	
	public void startUnlock(GarageDoorInterface garageDoor) {
		
		System.out.println("Garage State will be changed to GarageAwaitingCombination...");
		garageDoor.setGarageState(GarageAwaitingCombination.enter());
	}


	
	public void close(GarageDoorInterface garageDoor) {

		System.out.println("Garage is in locked state.");
		
	}


	public void open(GarageDoorInterface garageDoor) {
		System.out.println("Garage is in locked state.");
	}


	public void lock(GarageDoorInterface garageDoor) {

		System.out.println("Garage is in locked state.");
	}



	public void errorEntered(GarageDoorInterface garageDoor) {
		System.out.println("Garage is in locked state.");
	}

	public void combinationEntered(GarageDoorInterface garageDoor) {
		
		System.out.println("Garage is in locked state.");
	}

}
