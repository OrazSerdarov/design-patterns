package state_pattern;

 interface GarageDoorInterface {
	

	
	public abstract void close();
	public abstract  void open(); 
	public abstract  void lock(); 
	public abstract  void startUnlock();
	public abstract  void errorEntered();
	public abstract  void combinationEntered(); 
	public abstract GarageState getCurrentState();
	public abstract void setGarageState(GarageState s);
	

	
}



