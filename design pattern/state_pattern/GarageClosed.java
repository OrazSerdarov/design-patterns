package state_pattern;

public class GarageClosed  implements GarageState{
	

	private static GarageState garageClosed  = null;
	
	private GarageClosed() {
		System.out.println("GarageClosed State created ! ");
		
	}
	
	public static GarageState enter() {

		if(garageClosed ==null)
			garageClosed=new GarageClosed();
		
		return garageClosed;
	}


	public void open(GarageDoorInterface garageDoor ) {
		System.out.println("Garage opens...");
		garageDoor.setGarageState(GarageOpened.enter());
		
	}
	public void lock(GarageDoorInterface garageDoor) {
		
		System.out.println("Garage will be locked now..");
		garageDoor.setGarageState(GarageLocked.enter());
		
	}

	public void startUnlock(GarageDoorInterface garageDoor) {
		System.out.println("Garage is in closed state.");
		
		
	}

	public void errorEntered(GarageDoorInterface garageDoor) {
		System.out.println("Garage is in closed state.");
		
	}

	public void combinationEntered(GarageDoorInterface garageDoor) {
		System.out.println("Garage is in closed state.");
	}

	public void close(GarageDoorInterface garageDoor) {

		System.out.println("Garage is already closed");
	}

}
