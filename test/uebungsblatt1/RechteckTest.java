package uebungsblatt1;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import uebungsblatt1.aufgabe2.Rechteck;

class RechteckTest {
	
	public static Rechteck r = null;

	@Test
	@Tag("First")
	public void testeUmfang() {
		r = new Rechteck(10,20);
		assertEquals(60, r.berechneUmfang());
		

	}
	
	@Test
	public void testInhalt() {
		r = new Rechteck(10,20);
		assertEquals(200, r.berechneInhalt());
	}
	


}
